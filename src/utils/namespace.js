function mapValues ( obj, f ) {
    const res = {};
    Object.keys(obj).forEach( key => {
        res[key] = f(obj[key], key)
    } );
    return res;
}

export default (module, types) => {
    let newObject = {};
    mapValues(types, (names, type) => {
        newObject[type] = {};
        types[type].forEach( name => {
            newObject[type][name] = module + ':' + name;
        } )
    } )
    return newObject;
}