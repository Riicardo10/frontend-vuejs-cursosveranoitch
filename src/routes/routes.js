import Vue    from 'vue';
import Router from 'vue-router'
Vue.use( Router );

import Login        from '@/components/Auth/Login';
import General      from '@/components/General';
import Area         from '@/components/Area/Area';
import Carrera      from '@/components/Carrera/Carrera';
import Semestre     from '@/components/Semestre/Semestre';
import Profesor     from '@/components/Profesor/Profesor';
import Rol          from '@/components/Rol/Rol';
import Estudiante   from '@/components/Estudiante/Estudiante';
import Materia      from '@/components/Materia/Materia';
import Usuario      from '@/components/Usuario/Usuario';
import MateriasSolicitadasJefe          from '@/components/MateriasSolicitadas/MateriasSolicitadasJefe';
import MateriasSolicitadasJefeLista     from '@/components/MateriasSolicitadas/MateriasSolicitadasJefeLista';
import MateriasSolicitadasCoordinador   from '@/components/MateriasSolicitadas/MateriasSolicitadasCoordinador';
import MateriasSolicitadasCoordinadorLista   from '@/components/MateriasSolicitadas/MateriasSolicitadasCoordinadorLista';
import PlanEstudioCoordinador           from '@/components/MateriasSolicitadas/PlanEstudioCoordinador';
import Curriculum                       from '@/components/Curriculum/Curriculum';


import {store} from '@/main'

const router = new Router( {
    routes: [
        {
            path: '/',
            name: 'general',
            component: General,
            meta: { Auth: false, title: 'General' },
            beforeEnter: (to, from, next) => {
                if( !store.state.authModule.autenticado ) 
                    next( {path: '/login'} );
                else
                    next();
            }
        },
        {
            path: '/area',
            name: 'area',
            component: Area,
            meta: { Auth: false, title: 'Area' },
            beforeEnter: (to, from, next) => {
                let credenciales = JSON.parse( store.state.authModule.credenciales ) ;
                if( !store.state.authModule.autenticado || credenciales.id_rol != 1 )
                    next( {path: '/login'} );
                else
                    next();
            },
            
        },
        {
            path: '/carrera',
            name: 'carrera',
            component: Carrera,
            meta: { Auth: false, title: 'Carrera' },
            beforeEnter: (to, from, next) => {
                let credenciales = JSON.parse( store.state.authModule.credenciales ) ;
                if( !store.state.authModule.autenticado || credenciales.id_rol != 1 )
                    next( {path: '/login'} );
                else
                    next();
            }
        },
        {
            path: '/usuario',
            name: 'usuario',
            component: Usuario,
            meta: { Auth: false, title: 'Usuario' },
            beforeEnter: (to, from, next) => {
                let credenciales = JSON.parse( store.state.authModule.credenciales ) ;
                if( !store.state.authModule.autenticado || credenciales.id_rol != 1 )
                    next( {path: '/login'} );
                else
                    next();
            }
        },
        {
            path: '/semestre',
            name: 'semestre',
            component: Semestre,
            meta: { Auth: false, title: 'Semestre' },
            beforeEnter: (to, from, next) => {
                let credenciales = JSON.parse( store.state.authModule.credenciales ) ;
                if( !store.state.authModule.autenticado || credenciales.id_rol != 1 )
                    next( {path: '/login'} );
                else
                    next();
            }
        },
        {
            path: '/rol',
            name: 'rol',
            component: Rol,
            meta: { Auth: false, title: 'Rol' },
            beforeEnter: (to, from, next) => {
                let credenciales = JSON.parse( store.state.authModule.credenciales ) ;
                if( !store.state.authModule.autenticado || credenciales.id_rol != 1 )
                    next( {path: '/login'} );
                else
                    next();
            }
        },
        {
            path: '/profesor',
            name: 'profesor',
            component: Profesor,
            meta: { Auth: false, title: 'Profesor' },
            beforeEnter: (to, from, next) => {
                let credenciales = JSON.parse( store.state.authModule.credenciales ) ;
                if( !store.state.authModule.autenticado || credenciales.id_rol != 1 )
                    next( {path: '/login'} );
                else
                    next();
            }
        },
        {
            path: '/estudiante',
            name: 'estudiante',
            component: Estudiante,
            meta: { Auth: false, title: 'Estudiante' },
            beforeEnter: (to, from, next) => {
                let credenciales = JSON.parse( store.state.authModule.credenciales ) ;
                if( !store.state.authModule.autenticado || credenciales.id_rol != 1 )
                    next( {path: '/login'} );
                else
                    next();
            }
        },
        {
            path: '/materia',
            name: 'materia',
            component: Materia,
            meta: { Auth: false, title: 'Materia' },
            beforeEnter: (to, from, next) => {
                let credenciales = JSON.parse( store.state.authModule.credenciales ) ;
                if( !store.state.authModule.autenticado || credenciales.id_rol != 1 )
                    next( {path: '/login'} );
                else
                    next();
            }
        },
        {
            path: '/materias-solicitadas-jefe',
            name: 'materias-solicitadas-jefe',
            component: MateriasSolicitadasJefe,
            meta: { Auth: false, title: 'Materias solicitadas' },
            beforeEnter: (to, from, next) => {
                let credenciales = JSON.parse( store.state.authModule.credenciales ) ;
                if( !store.state.authModule.autenticado || credenciales.id_rol != 3 )
                    next( {path: '/login'} );
                else
                    next();
            }
        },
        {
            path: '/materias-solicitadas-jefe/lista/:id',
            name: 'materias-solicitadas-jefe-lista',
            component: MateriasSolicitadasJefeLista,
            meta: { Auth: false, title: 'Lista de la materia' },
            beforeEnter: (to, from, next) => {
                let credenciales = JSON.parse( store.state.authModule.credenciales ) ;
                if( !store.state.authModule.autenticado || credenciales.id_rol != 3)
                    next( {path: '/login'} );
                else
                    next();
            }
        },
        {
            path: '/materias-solicitadas-coordinador/lista/:id/:clave/:materia',
            name: 'materias-solicitadas-coordinador-lista',
            component: MateriasSolicitadasCoordinadorLista,
            meta: { Auth: false, title: 'Lista de la materia' },
            beforeEnter: (to, from, next) => {
                let credenciales = JSON.parse( store.state.authModule.credenciales ) ;
                if( !store.state.authModule.autenticado || credenciales.id_rol != 2)
                    next( {path: '/login'} );
                else
                    next();
            }
        },
        {
            path: '/plan-estudio',
            name: 'plan-estudio',
            component: PlanEstudioCoordinador,
            meta: { Auth: false, title: 'Plan de estudio' },
            beforeEnter: (to, from, next) => {
                let credenciales = JSON.parse( store.state.authModule.credenciales ) ;
                if( !store.state.authModule.autenticado || credenciales.id_rol != 2 )
                    next( {path: '/login'} );
                else
                    next();
            }
        },
        {
            path: '/curriculum/:id',
            name: 'curriculum',
            component: Curriculum,
            meta: { Auth: false, title: 'Curriculum' },
            beforeEnter: (to, from, next) => {
                let credenciales = JSON.parse( store.state.authModule.credenciales ) ;
                if( !store.state.authModule.autenticado || credenciales.id_rol != 2 )
                    next( {path: '/login'} );
                else
                    next();
            }
        },
        {
            path: '/materias-solicitadas-coordinador',
            name: 'materias-solicitadas-coordinador',
            component: MateriasSolicitadasCoordinador,
            meta: { Auth: false, title: 'Materias solicitadas' },
            beforeEnter: (to, from, next) => {
                let credenciales = JSON.parse( store.state.authModule.credenciales ) ;
                if( !store.state.authModule.autenticado || credenciales.id_rol != 2 )
                    next( {path: '/login'} );
                else
                    next();
            }
        },
        {
            path: '/login',
            name: 'login',
            component: Login,
            meta: { Auth: false, title: 'Login' },
            beforeEnter: (to, from, next) => {
                if( store.state.authModule.autenticado ) 
                    next( {path: '/'} );
                else
                    next();
            }
        },
    ]
} );

router.beforeEach( (to, from, next) => {
    document.title = to.meta.title;
    if( to.meta.Auth && !store.state.authModule.logged ) {
        next( {path: '/login'} );
    }
    else{
        if( store.state.authModule.logged ) {
            store.commit(authTypes.mutations.setUsuario);
        }
        next();
    }
} );

export default router;