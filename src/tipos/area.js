import namespace from '@/utils/namespace';

export default namespace( 'area', {
    getters: [
        'area',
        'areas',
        'paginacion'
    ],
    actions: [
        'registrar',
        'actualizar',
        'fetchAreas'
    ],
    mutations: [
        'areasRecibidas',
        'setArea',
        'setAreas',
        'setPaginacion'
    ]
} );