import namespace from '@/utils/namespace';

export default namespace( 'materia_solicitada_jefe', {
    getters: [
        'materias',
        'materias_solicitadas_jefe',
        'paginacion',
        'materia_solicitada',
        'profesores',
        'lista'
    ],
    actions: [
        'fetchMateriasSolicitadasJefe',
        'fetchMateriaSolicitadaJefe',
        'fetchProfesores',
        'aprobarMateria',
        'fetchListaMateria'
    ],
    mutations: [
        'setMaterias',
        'setMateriasSolicitadasJefe',
        'setPaginacion',
        'setMateriaSolicitada',
        'setProfesores',
        'setLista'
    ]
} );