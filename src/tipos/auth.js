import namespace from '@/utils/namespace';

export default namespace( 'auth', {
    getters: [
        'usuario',
        'autenticado',
        'credenciales'
    ],
    actions: [
        'login',
        'logout',
        'registro',
        'actualizarPerfil'
    ],
    mutations: [
        'setUsuario',
        'setAutenticado'
    ]
} );