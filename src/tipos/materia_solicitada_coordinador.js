import namespace from '@/utils/namespace';

export default namespace( 'materia_solicitada_coordinador', {
    getters: [
        'carreras',
        'curriculum',
        'cantidad_materias',
        'materias_solicitadas',
        'semestres',
        'estudiante',
        'cantidad_estudiantes'
    ],
    actions: [
        'fetchCarreras',
        'fetchCantidadMateriasSolicitadas',
        'fetchMateriasSolicitadas',
        'fetchCurriculum',
        'aumentarCantidad',
        'disminuirCantidad',
        'fetchRegistrarMateriasSolicitadas',
        'fetchEstudiante',
        'fetchRegistrarListado'
    ],
    mutations: [
        'setCarreras',
        'setCantidadMaterias',
        'setMateriasSolicitadas',
        'setCurriculum',
        'setSemestres',
        'setCantidad',
        'setCantidadMenos',
        'setCantidad',
        'setEstudiante',
        'setCantidadEstudiantes'
    ]
} );