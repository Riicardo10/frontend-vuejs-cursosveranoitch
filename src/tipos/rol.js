import namespace from '@/utils/namespace';

export default namespace( 'rol', {
    getters: [
        'rol',
        'roles',
        'paginacion'
    ],
    actions: [
        'registrar',
        'actualizar',
        'fetchRoles',
        'activar'
    ],
    mutations: [
        'rolesRecibidos',
        'setRol',
        'setRoles',
        'setPaginacion'
    ]
} );