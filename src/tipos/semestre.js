import namespace from '@/utils/namespace';

export default namespace( 'semestre', {
    getters: [
        'semestre',
        'semestres',
        'paginacion'
    ],
    actions: [
        'registrar',
        'actualizar',
        'fetchSemestres'
    ],
    mutations: [
        'semestresRecibidos',
        'setSemestre',
        'setSemestres',
        'setPaginacion'
    ]
} );