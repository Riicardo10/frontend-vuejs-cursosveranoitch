import namespace from '@/utils/namespace';

export default namespace( 'profesor', {
    getters: [
        'profesor',
        'profesores',
        'paginacion',
        'areas'
    ],
    actions: [
        'registrar',
        'actualizar',
        'fetchProfesores',
        'fetchAreas',
        'activar'
    ],
    mutations: [
        'profesoresRecibidos',
        'setProfesor',
        'setProfesores',
        'setAreas',
        'setPaginacion'
    ]
} );