import namespace from '@/utils/namespace';

export default namespace( 'materia', {
    getters: [
        'materia',
        'materias',
        'semestres',
        'carreras',
        'paginacion'
    ],
    actions: [
        'registrar',
        'actualizar',
        'fetchMaterias',
        'fetchCarreras',
        'fetchSemestres',
    ],
    mutations: [
        'materiasRecibidas',
        'setMateria',
        'setMaterias',
        'setSemestres',
        'setCarreras',
        'setPaginacion'
    ]
} );