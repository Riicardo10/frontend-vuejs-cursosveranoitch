import namespace from '@/utils/namespace';

export default namespace( 'global', {
    actions: [],
    getters: [
        'procesando',
        'ruta'
    ],
    mutations: [
        'iniciarProceso',
        'terminarProceso',
    ]
} );