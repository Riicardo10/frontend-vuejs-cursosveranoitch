import namespace from '@/utils/namespace';

export default namespace( 'usuario', {
    getters: [
        'usuario',
        'usuarios',
        'roles',
        'paginacion'
    ],
    actions: [
        'registrar',
        'actualizar',
        'fetchUsuarios',
        'fetchRoles',
        'activar',
    ],
    mutations: [
        'usuariosRecibidos',
        'setUsuario',
        'setUsuarios',
        'setRoles',
        'setPaginacion'
    ]
} );