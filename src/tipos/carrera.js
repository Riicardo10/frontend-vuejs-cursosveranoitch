import namespace from '@/utils/namespace';

export default namespace( 'carrera', {
    getters: [
        'carrera',
        'carreras',
        'paginacion'
    ],
    actions: [
        'registrar',
        'actualizar',
        'fetchCarreras'
    ],
    mutations: [
        'carrerasRecibidas',
        'setCarrera',
        'setCarreras',
        'setPaginacion'
    ]
} );