import namespace from '@/utils/namespace';

export default namespace( 'estudiante', {
    getters: [
        'estudiante',
        'estudiantes',
        'carreras',
        'paginacion'
    ],
    actions: [
        'registrar',
        'actualizar',
        'fetchCarreras',
        'fetchEstudiantes',
    ],
    mutations: [
        'estudiantesRecibidos',
        'setEstudiante',
        'setEstudiantes',
        'setPaginacion',
        'setCarreras'
    ]
} );