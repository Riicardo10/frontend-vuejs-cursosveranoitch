import Vue from 'vue'
import App from './App.vue'
import router from '@/routes/routes';

import VueResource from 'vue-resource';
Vue.use( VueResource );
Vue.http.options.root = 'http://192.168.1.67:8000/';
Vue.http.interceptors.push( (request, next) => {
    // request.headers.set( 'Authorization', 'Bearer ' + window.localStorage.getItem( '_token' ) );
    next();
} );

import Vuex from 'vuex';
Vue.use( Vuex );

import BlockUI from 'vue-blockui';
Vue.use( BlockUI );

import tiposGlobal      from '@/tipos/global';
import authModule       from '@/modules/auth';
import areaModule       from '@/modules/area';
import carreraModule    from '@/modules/carrera';
import semestreModule   from '@/modules/semestre';
import profesorModule   from '@/modules/profesor';
import rolModule        from '@/modules/rol';
import estudianteModule from '@/modules/estudiante';
import usuarioModule    from '@/modules/usuario';
import materiaModule    from '@/modules/materia';
import materiaSolicitadaJefeModule  from '@/modules/materia_solicitada_jefe';
import materiaSolicitadaCoordinadorModule  from '@/modules/materia_solicitada_coordinador';

import VeeValidate from 'vee-validate';
Vue.use( VeeValidate );

import {ClientTable} from 'vue-tables-2';   
Vue.use( ClientTable, {}, false, 'bootstrap3', 'default' );

export const store = new Vuex.Store( {
    state: {
        procesando: false,
    }, 
    actions: {
    },
    getters: {
        [tiposGlobal.getters.procesando]:   (state) => state.procesando
    },
    mutations: {
        [tiposGlobal.mutations.iniciarProceso] (state) {
            state.procesando = true;
        },
        [tiposGlobal.mutations.terminarProceso] (state) {
            state.procesando = false;
        }
    },
    modules:   {
        authModule,
        areaModule,
        carreraModule,
        semestreModule,
        profesorModule,
        rolModule,
        estudianteModule,
        usuarioModule,
        materiaModule,
        materiaSolicitadaJefeModule,
        materiaSolicitadaCoordinadorModule
    }
} );

new Vue({
  el: '#app',
  render: h => h(App),
  store,
  router
})
