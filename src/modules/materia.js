import tiposGlobal from '@/tipos/global';
import tiposMateria from '@/tipos/materia';
import Vue from 'vue';

const state = {
  materia: null,
  materias: [],
  semestres: [],
  carreras: [],
  paginacion: []
};

const actions = {
    [tiposMateria.actions.fetchMaterias]: ({commit}, data) => {
        commit( tiposGlobal.mutations.iniciarProceso );
        let url = data.buscar ? 'materia?page=' + data.pagina + '&buscar=' + data.buscar : 'materia?page=' + data.pagina;        
        Vue.http.get( url )
            .then( (result) => {
                commit( tiposMateria.mutations.setMaterias, { apiResponse: result.body.materias.data } );
                commit( tiposMateria.mutations.setPaginacion, { apiResponse: result.body.paginacion } );
            });
            commit( tiposGlobal.mutations.terminarProceso )
    },
    [tiposMateria.actions.fetchSemestres]: ({commit}, data) => {
        commit( tiposGlobal.mutations.iniciarProceso );
        Vue.http.get( 'semestres' )
            .then( (result) => {
                commit( tiposMateria.mutations.setSemestres, { apiResponse: result.body.semestres } );
            });
            commit( tiposGlobal.mutations.terminarProceso )
    },
    [tiposMateria.actions.fetchCarreras]: ({commit}, data) => {
        commit( tiposGlobal.mutations.iniciarProceso );
        Vue.http.get( 'carreras' )
            .then( (result) => {
                commit( tiposMateria.mutations.setCarreras, { apiResponse: result.body.carreras } );
            });
            commit( tiposGlobal.mutations.terminarProceso )
    },
    [tiposMateria.actions.registrar]: ({commit}, data) => {
        commit(tiposGlobal.mutations.iniciarProceso);
        return new Promise( (resolve, reject) => {
            Vue.http.post( 'materia', {materia:data} )
                .then( (result) => {
                    resolve( result );
                } )
                .catch( err => {
                    reject( err );
                } )
                .finally( () => {
                    commit(tiposGlobal.mutations.terminarProceso);
                } );
        } );
    },
    [tiposMateria.actions.actualizar]: ({commit}, data) => {
        commit(tiposGlobal.mutations.iniciarProceso);
        return new Promise( (resolve, reject) => {
            Vue.http.put( 'materia/' + data.clave, data )
                .then( (result) => {
                    resolve( result );
                } )
                .catch( err => {
                    reject( err );
                } )
                .finally( () => {
                    commit(tiposGlobal.mutations.terminarProceso);
                } );
        } );
    },
    [tiposMateria.actions.eliminar]: ({commit}) => {},    
};

const getters = {
    [tiposMateria.getters.materia]: (state) => {
        return state.materia;
    },
    [tiposMateria.getters.semestres]: (state) => {
        return state.semestres;
    },
    [tiposMateria.getters.carreras]: (state) => {
        return state.carreras;
    },
    [tiposMateria.getters.materias]: (state) => {
        return state.materias;
    },
    [tiposMateria.getters.paginacion]: (state) => {
        return state.paginacion;
    },
};

const mutations = {
    [tiposMateria.mutations.setMaterias]: (state, materias) => {
        state.materias = materias.apiResponse;
    },
    [tiposMateria.mutations.setSemestres]: (state, semestres) => {
        state.semestres = semestres.apiResponse;
    },
    [tiposMateria.mutations.setCarreras]: (state, carreras) => {
        state.carreras = carreras.apiResponse;
    },
    [tiposMateria.mutations.setMateria]: (state) => {
        state.materia = materia;
    },
    [tiposMateria.mutations.setPaginacion]: (state, paginacion) => {
        state.paginacion = paginacion.apiResponse;
    }
};

export default {
    state,
    actions,
    getters,
    mutations
};
