import tiposGlobal from '@/tipos/global';
import tiposRol from '@/tipos/rol';
import Vue from 'vue';

const state = {
  rol: null,
  roles: [],
  paginacion: []
};

const actions = {
    [tiposRol.actions.fetchRoles]: ({commit}, data) => {
        commit( tiposGlobal.mutations.iniciarProceso );
        let url = data.buscar ? 'rol?page=' + data.pagina + '&buscar=' + data.buscar : 'rol?page=' + data.pagina;        
        Vue.http.get( url )
            .then( (result) => {
                commit( tiposRol.mutations.setRoles, { apiResponse: result.body.roles.data } );
                commit( tiposRol.mutations.setPaginacion, { apiResponse: result.body.paginacion } );
            });
            commit( tiposGlobal.mutations.terminarProceso )
    },
    [tiposRol.actions.registrar]: ({commit}, data) => {
        commit(tiposGlobal.mutations.iniciarProceso);
        return new Promise( (resolve, reject) => {
            Vue.http.post( 'rol', {rol:data} )
                .then( (result) => {
                    resolve( result );
                } )
                .catch( err => {
                    reject( err );
                } )
                .finally( () => {
                    commit(tiposGlobal.mutations.terminarProceso);
                } );
        } );
    },
    [tiposRol.actions.actualizar]: ({commit}, data) => {
        commit(tiposGlobal.mutations.iniciarProceso);
        return new Promise( (resolve, reject) => {
            Vue.http.put( 'rol/' + data.id, data )
                .then( (result) => {
                    console.log(result)
                    resolve( result );
                } )
                .catch( err => {
                    reject( err );
                } )
                .finally( () => {
                    commit(tiposGlobal.mutations.terminarProceso);
                } );
        } );
    },
    [tiposRol.actions.eliminar]: ({commit}) => {},    
    [tiposRol.actions.activar]: ({commit}, data) => {
        let url = data.estado == '1' ? 'rol/activar/' + data.id : 'rol/desactivar/' + data.id;
        commit( tiposGlobal.mutations.iniciarProceso );
        return new Promise( (resolve, reject) => {
            Vue.http.put( url )
                .then( (result) => {
                    resolve( result );
                } )
                .catch( err => {
                    reject( err );
                } )
                .finally( () => {
                    commit(tiposGlobal.mutations.terminarProceso);
                } );
        } );
    },    
};

const getters = {
    [tiposRol.getters.rol]: (state) => {
        return state.rol;
    },
    [tiposRol.getters.roles]: (state) => {
        return state.roles;
    },
    [tiposRol.getters.paginacion]: (state) => {
        return state.paginacion;
    },
};

const mutations = {
    [tiposRol.mutations.setRoles]: (state, roles) => {
        state.roles = roles.apiResponse;
    },
    [tiposRol.mutations.setRol]: (state) => {
        state.rol = rol;
    },
    [tiposRol.mutations.setPaginacion]: (state, paginacion) => {
        state.paginacion = paginacion.apiResponse;
    }
};

export default {
    state,
    actions,
    getters,
    mutations
};
