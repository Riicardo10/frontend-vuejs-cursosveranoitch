import tiposGlobal from '@/tipos/global';
import tiposArea from '@/tipos/area';
import Vue from 'vue';

const state = {
  area: null,
  areas: [],
  paginacion: []
};

const actions = {
    [tiposArea.actions.fetchAreas]: ({commit}, data) => {
        commit( tiposGlobal.mutations.iniciarProceso );
        let url = data.buscar ? 'area?page=' + data.pagina + '&buscar=' + data.buscar : 'area?page=' + data.pagina;        
        Vue.http.get( url )
            .then( (result) => {
                commit( tiposArea.mutations.setAreas, { apiResponse: result.body.areas.data } );
                commit( tiposArea.mutations.setPaginacion, { apiResponse: result.body.paginacion } );
            });
            commit( tiposGlobal.mutations.terminarProceso )
    },
    [tiposArea.actions.registrar]: ({commit}, data) => {
        commit(tiposGlobal.mutations.iniciarProceso);
        return new Promise( (resolve, reject) => {
            Vue.http.post( 'area', {area:data} )
                .then( (result) => {
                    resolve( result );
                } )
                .catch( err => {
                    reject( err );
                } )
                .finally( () => {
                    commit(tiposGlobal.mutations.terminarProceso);
                } );
        } );
    },
    [tiposArea.actions.actualizar]: ({commit}, data) => {
        commit(tiposGlobal.mutations.iniciarProceso);
        return new Promise( (resolve, reject) => {
            Vue.http.put( 'area/' + data.id, data )
                .then( (result) => {
                    resolve( result );
                } )
                .catch( err => {
                    reject( err );
                } )
                .finally( () => {
                    commit(tiposGlobal.mutations.terminarProceso);
                } );
        } );
    },
    [tiposArea.actions.eliminar]: ({commit}) => {},    
};

const getters = {
    [tiposArea.getters.area]: (state) => {
        return state.area;
    },
    [tiposArea.getters.areas]: (state) => {
        return state.areas;
    },
    [tiposArea.getters.paginacion]: (state) => {
        return state.paginacion;
    },
};

const mutations = {
    [tiposArea.mutations.setAreas]: (state, areas) => {
        state.areas = areas.apiResponse;
    },
    [tiposArea.mutations.setArea]: (state) => {
        state.area = area;
    },
    [tiposArea.mutations.setPaginacion]: (state, paginacion) => {
        state.paginacion = paginacion.apiResponse;
    }
};

export default {
    state,
    actions,
    getters,
    mutations
};
