import tiposGlobal from '@/tipos/global';
import tiposSemestre from '@/tipos/semestre';
import Vue from 'vue';

const state = {
  semestre: null,
   semestres: [],
  paginacion: []
};

const actions = {
    [tiposSemestre.actions.fetchSemestres]: ({commit}, data) => {
        commit( tiposGlobal.mutations.iniciarProceso );
        let url = data.buscar ? 'semestre?page=' + data.pagina + '&buscar=' + data.buscar : 'semestre?page=' + data.pagina;        
        Vue.http.get( url )
            .then( (result) => {
                commit( tiposSemestre.mutations.setSemestres, { apiResponse: result.body.semestres.data } );
                commit( tiposSemestre.mutations.setPaginacion, { apiResponse: result.body.paginacion } );
            });
            commit( tiposGlobal.mutations.terminarProceso )
    },
    [tiposSemestre.actions.registrar]: ({commit}, data) => {
        commit(tiposGlobal.mutations.iniciarProceso);
        return new Promise( (resolve, reject) => {
            Vue.http.post( 'semestre', {semestre:data} )
                .then( (result) => {
                    resolve( result );
                } )
                .catch( err => {
                    reject( err );
                } )
                .finally( () => {
                    commit(tiposGlobal.mutations.terminarProceso);
                } );
        } );
    },
    [tiposSemestre.actions.actualizar]: ({commit}, data) => {
        commit(tiposGlobal.mutations.iniciarProceso);
        return new Promise( (resolve, reject) => {
            Vue.http.put( 'semestre/' + data.id, data )
                .then( (result) => {
                    resolve( result );
                } )
                .catch( err => {
                    reject( err );
                } )
                .finally( () => {
                    commit(tiposGlobal.mutations.terminarProceso);
                } );
        } );
    },
    [tiposSemestre.actions.eliminar]: ({commit}) => {},    
};

const getters = {
    [tiposSemestre.getters.semestre]: (state) => {
        return state.semestre;
    },
    [tiposSemestre.getters.semestres]: (state) => {
        return state.semestres;
    },
    [tiposSemestre.getters.paginacion]: (state) => {
        return state.paginacion;
    },
};

const mutations = {
    [tiposSemestre.mutations.setSemestres]: (state, semestres) => {
        state.semestres = semestres.apiResponse;
    },
    [tiposSemestre.mutations.setSemestre]: (state) => {
        state.semestre = semestre;
    },
    [tiposSemestre.mutations.setPaginacion]: (state, paginacion) => {
        state.paginacion = paginacion.apiResponse;
    }
};

export default {
    state,
    actions,
    getters,
    mutations
};
