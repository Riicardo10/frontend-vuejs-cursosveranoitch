import tiposGlobal from '@/tipos/global';
import tiposMateriaSolicitadaCoordinador from '@/tipos/materia_solicitada_coordinador';
import Vue from 'vue';

const state = {
    carreras: [],
    cantidad_materias: 0,
    materias_solicitadas: [],
    curriculum: [],
    semestres: 0,
    estudiante: {},
    cantidad_estudiantes: 0,
};

const actions = {
    [tiposMateriaSolicitadaCoordinador.actions.fetchCarreras]: ({commit}, data) => {
        commit( tiposGlobal.mutations.iniciarProceso );
        Vue.http.get( 'carreras' )
            .then( (result) => {
                commit( tiposMateriaSolicitadaCoordinador.mutations.setCarreras, { apiResponse: result.body.carreras } );
            });
            commit( tiposGlobal.mutations.terminarProceso )
    },
    [tiposMateriaSolicitadaCoordinador.actions.fetchCantidadMateriasSolicitadas]: ({commit}, data) => {
        commit( tiposGlobal.mutations.iniciarProceso );
        Vue.http.get( 'cantidad-materias/' + data )
            .then( (result) => {
                commit( tiposMateriaSolicitadaCoordinador.mutations.setCantidadMaterias, { apiResponse: result.body.cantidad } );
            });
            commit( tiposGlobal.mutations.terminarProceso )
    },
    [tiposMateriaSolicitadaCoordinador.actions.fetchMateriasSolicitadas]: ({commit}, data) => {
        commit( tiposGlobal.mutations.iniciarProceso );
        Vue.http.get( 'materias_solicitadas/' + data )
            .then( (result) => {
                commit( tiposMateriaSolicitadaCoordinador.mutations.setMateriasSolicitadas, { apiResponse: result.body.materias } );
            });
            commit( tiposGlobal.mutations.terminarProceso )
    },
    [tiposMateriaSolicitadaCoordinador.actions.fetchCurriculum]: ({commit}, data) => {
        commit( tiposGlobal.mutations.iniciarProceso );
        Vue.http.get( 'curriculum/' + data )
            .then( (result) => {
                commit( tiposMateriaSolicitadaCoordinador.mutations.setCurriculum, { apiResponse: result.body.curriculum } );
                commit( tiposMateriaSolicitadaCoordinador.mutations.setSemestres, { apiResponse: result.body.semestres } );
            });
            commit( tiposGlobal.mutations.terminarProceso )
    },
    [tiposMateriaSolicitadaCoordinador.actions.aumentarCantidad]: ({commit}) => {
        commit( tiposMateriaSolicitadaCoordinador.mutations.setCantidad );
    },
    [tiposMateriaSolicitadaCoordinador.actions.disminuirCantidad]: ({commit}) => {
        commit( tiposMateriaSolicitadaCoordinador.mutations.setCantidadMenos );
    },
    [tiposMateriaSolicitadaCoordinador.actions.fetchRegistrarMateriasSolicitadas]: ({commit}, data) => {
        commit(tiposGlobal.mutations.iniciarProceso);
        return new Promise( (resolve, reject) => {
            Vue.http.post( 'materia_solicitada', {datos:data} )
                .then( (result) => {
                    resolve( result );
                } )
                .catch( err => {
                    reject( err );
                } )
                .finally( () => {
                    commit(tiposGlobal.mutations.terminarProceso);
                } );
        } );
    },
    [tiposMateriaSolicitadaCoordinador.actions.fetchEstudiante]: ({commit}, data) => {
        commit(tiposGlobal.mutations.iniciarProceso);
        return new Promise( (resolve, reject) => {
            Vue.http.get( 'estudiantes/' + data )
                .then( (result) => {
                    commit( tiposMateriaSolicitadaCoordinador.mutations.setEstudiante, { apiResponse: result.body.estudiante } );
                } )
                .catch( err => {
                    reject( err );
                } )
                .finally( () => {
                    commit(tiposGlobal.mutations.terminarProceso);
                } );
        } );
    },
    [tiposMateriaSolicitadaCoordinador.actions.fetchRegistrarListado]: ({commit}, data) => {
        commit(tiposGlobal.mutations.iniciarProceso);
        return new Promise( (resolve, reject) => {
            Vue.http.post( 'lista_materia', data )
                .then( (result) => {
                    resolve( result );
                } )
                .catch( err => {
                    reject( err );
                } )
                .finally( () => {
                    commit(tiposGlobal.mutations.terminarProceso);
                } );
        } );
    },
};

const getters = {
    [tiposMateriaSolicitadaCoordinador.getters.carreras]: (state) => {
        return state.carreras;
    },
    [tiposMateriaSolicitadaCoordinador.getters.cantidad_materias]: (state) => {
        return state.cantidad_materias;
    },
    [tiposMateriaSolicitadaCoordinador.getters.materias_solicitadas]: (state) => {
        return state.materias_solicitadas;
    },
    [tiposMateriaSolicitadaCoordinador.getters.curriculum]: (state) => {
        return state.curriculum;
    },
    [tiposMateriaSolicitadaCoordinador.getters.semestres]: (state) => {
        return state.semestres;
    },
    [tiposMateriaSolicitadaCoordinador.getters.estudiante]: (state) => {
        return state.estudiante;
    },
};

const mutations = {
    [tiposMateriaSolicitadaCoordinador.mutations.setCarreras]: (state, carreras) => {
        state.carreras = carreras.apiResponse;
    },
    [tiposMateriaSolicitadaCoordinador.mutations.setCantidadMaterias]: (state, cantidad) => {
        state.cantidad_materias = cantidad.apiResponse;
    },
    [tiposMateriaSolicitadaCoordinador.mutations.setMateriasSolicitadas]: (state, materias) => {
        state.materias_solicitadas = materias.apiResponse;
    },
    [tiposMateriaSolicitadaCoordinador.mutations.setCurriculum]: (state, curriculum) => {
        state.curriculum = curriculum.apiResponse;
    },
    [tiposMateriaSolicitadaCoordinador.mutations.setSemestres]: (state, semestres) => {
        state.semestres = semestres.apiResponse;
    },
    [tiposMateriaSolicitadaCoordinador.mutations.setCantidad]: (state) => {
        state.cantidad_materias = state.cantidad_materias + 1;
    },
    [tiposMateriaSolicitadaCoordinador.mutations.setCantidadMenos]: (state) => {
        state.cantidad_materias = state.cantidad_materias - 1;
    },
    [tiposMateriaSolicitadaCoordinador.mutations.setEstudiante]: (state, estudiante) => {
        state.estudiante = estudiante.apiResponse;
    },
};

export default {
    state,
    actions,
    getters,
    mutations
};
