import tiposGlobal from '@/tipos/global';
import tiposCarrera from '@/tipos/carrera';
import Vue from 'vue';

const state = {
  carrera: null,
  carreras: [],
  paginacion: []
};

const actions = {
    [tiposCarrera.actions.fetchCarreras]: ({commit}, data) => {
        commit( tiposGlobal.mutations.iniciarProceso );
        let url = data.buscar ? 'carrera?page=' + data.pagina + '&buscar=' + data.buscar : 'carrera?page=' + data.pagina;        
        Vue.http.get( url )
            .then( (result) => {
                commit( tiposCarrera.mutations.setCarreras, { apiResponse: result.body.carreras.data } );
                commit( tiposCarrera.mutations.setPaginacion, { apiResponse: result.body.paginacion } );
            });
            commit( tiposGlobal.mutations.terminarProceso )
    },
    [tiposCarrera.actions.registrar]: ({commit}, data) => {
        commit(tiposGlobal.mutations.iniciarProceso);
        return new Promise( (resolve, reject) => {
            Vue.http.post( 'carrera', {carrera:data} )
                .then( (result) => {
                    resolve( result );
                } )
                .catch( err => {
                    reject( err );
                } )
                .finally( () => {
                    commit(tiposGlobal.mutations.terminarProceso);
                } );
        } );
    },
    [tiposCarrera.actions.actualizar]: ({commit}, data) => {
        commit(tiposGlobal.mutations.iniciarProceso);
        return new Promise( (resolve, reject) => {
            Vue.http.put( 'carrera/' + data.id, data )
                .then( (result) => {
                    resolve( result );
                } )
                .catch( err => {
                    reject( err );
                } )
                .finally( () => {
                    commit(tiposGlobal.mutations.terminarProceso);
                } );
        } );
    },
    [tiposCarrera.actions.eliminar]: ({commit}) => {},    
};

const getters = {
    [tiposCarrera.getters.carrera]: (state) => {
        return state.carrera;
    },
    [tiposCarrera.getters.carreras]: (state) => {
        return state.carreras;
    },
    [tiposCarrera.getters.paginacion]: (state) => {
        return state.paginacion;
    },
};

const mutations = {
    [tiposCarrera.mutations.setCarreras]: (state, carreras) => {
        state.carreras = carreras.apiResponse;
    },
    [tiposCarrera.mutations.setCarrera]: (state) => {
        state.carrera = carrera;
    },
    [tiposCarrera.mutations.setPaginacion]: (state, paginacion) => {
        state.paginacion = paginacion.apiResponse;
    }
};

export default {
    state,
    actions,
    getters,
    mutations
};
