import tiposGlobal from '@/tipos/global';
import tiposUsuario from '@/tipos/usuario';
import Vue from 'vue';

const state = {
  usuario: null,
  usuarios: [],
  roles: [],
  paginacion: []
};

const actions = {
    [tiposUsuario.actions.fetchUsuarios]: ({commit}, data) => {
        let url = data.buscar ? 'usuario?page=' + data.pagina + '&buscar=' + data.buscar  : 'usuario?page=' + data.pagina;
        commit( tiposGlobal.mutations.iniciarProceso );
        Vue.http.get( url + '&criterio=' + data.criterio )
            .then( (result) => {
                commit( tiposUsuario.mutations.setUsuarios, { apiResponse: result.body.usuarios.data } );
                commit( tiposUsuario.mutations.setPaginacion, { apiResponse: result.body.paginacion } );
            });
            commit( tiposGlobal.mutations.terminarProceso )
    },
    [tiposUsuario.actions.fetchRoles]: ({commit}) => {
        commit( tiposGlobal.mutations.iniciarProceso );
        Vue.http.get( 'roles' )
            .then( (result) => {
                commit( tiposUsuario.mutations.setRoles, { apiResponse: result.body.roles } );
            });
            commit( tiposGlobal.mutations.terminarProceso )
    },
    [tiposUsuario.actions.registrar]: ({commit}, data) => {
        commit(tiposGlobal.mutations.iniciarProceso);
        return new Promise( (resolve, reject) => {
            Vue.http.post( 'usuario', {usuario:data} )
                .then( (result) => {
                    console.log(result)
                    resolve( result );
                } )
                .catch( err => {
                    reject( err );
                } )
                .finally( () => {
                    commit(tiposGlobal.mutations.terminarProceso);
                } );
        } );
    },
    [tiposUsuario.actions.actualizar]: ({commit}, data) => {
        commit(tiposGlobal.mutations.iniciarProceso);
        return new Promise( (resolve, reject) => {
            Vue.http.put( 'usuario/' + data.id, {usuario:data} )
                .then( (result) => {
                    console.log(result)
                    resolve( result );
                } )
                .catch( err => {
                    reject( err );
                } )
                .finally( () => {
                    commit(tiposGlobal.mutations.terminarProceso);
                } );
        } );
    },
    [tiposUsuario.actions.activar]: ({commit}, data) => {
        let url = data.estado == '1' ? 'usuario/activar/' + data.id : 'usuario/desactivar/' + data.id;
        commit( tiposGlobal.mutations.iniciarProceso );
        return new Promise( (resolve, reject) => {
            Vue.http.put( url )
                .then( (result) => {
                    resolve( result );
                } )
                .catch( err => {
                    reject( err );
                } )
                .finally( () => {
                    commit(tiposGlobal.mutations.terminarProceso);
                } );
        } );
    },    
    [tiposUsuario.actions.eliminar]: ({commit}) => {},    
};

const getters = {
    [tiposUsuario.getters.usuario]: (state) => {
        return state.usuario;
    },
    [tiposUsuario.getters.usuarios]: (state) => {
        return state.usuarios;
    },
    [tiposUsuario.getters.paginacion]: (state) => {
        return state.paginacion;
    },
    [tiposUsuario.getters.roles]: (state) => {
        return state.roles;
    },
};

const mutations = {
    [tiposUsuario.mutations.usuariosRecibidos]: (state, usuarios) => {
        state.usuarios = usuarios.apiResponse;
    },
    [tiposUsuario.mutations.setRoles]: (state, roles) => {
        state.roles = roles.apiResponse;
    },
    [tiposUsuario.mutations.setUsuario]: (state) => {
        state.usuario = usuario;
    },
    [tiposUsuario.mutations.setUsuarios]: (state, usuarios) => {
        state.usuarios = usuarios.apiResponse;
    },
    [tiposUsuario.mutations.setPaginacion]: (state, paginacion) => {
        state.paginacion = paginacion.apiResponse;
    }
};

export default {
    state,
    actions,
    getters,
    mutations
};