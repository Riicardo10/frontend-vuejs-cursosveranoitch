import tiposGlobal from '@/tipos/global';
import tiposProfesor from '@/tipos/profesor';
import Vue from 'vue';

const state = {
  profesor: null,
  profesores: [],
  areas: [],
  paginacion: []
};

const actions = {
    [tiposProfesor.actions.fetchProfesores]: ({commit}, data) => {
        commit( tiposGlobal.mutations.iniciarProceso );
        let url = data.buscar ? 'profesor?page=' + data.pagina + '&buscar=' + data.buscar : 'profesor?page=' + data.pagina;        
        Vue.http.get( url )
            .then( (result) => {
                commit( tiposProfesor.mutations.setProfesores, { apiResponse: result.body.profesores.data } );
                commit( tiposProfesor.mutations.setPaginacion, { apiResponse: result.body.paginacion } );
            });
            commit( tiposGlobal.mutations.terminarProceso )
    },
    [tiposProfesor.actions.fetchAreas]: ({commit}) => {
        commit( tiposGlobal.mutations.iniciarProceso );
        Vue.http.get( 'areas' )
            .then( (result) => {
                commit( tiposProfesor.mutations.setAreas, { apiResponse: result.body.areas } );
            });
            commit( tiposGlobal.mutations.terminarProceso )
    },
    [tiposProfesor.actions.registrar]: ({commit}, data) => {
        commit(tiposGlobal.mutations.iniciarProceso);
        return new Promise( (resolve, reject) => {
            Vue.http.post( 'profesor', {profesor:data} )
                .then( (result) => {
                    resolve( result );
                } )
                .catch( err => {
                    reject( err );
                } )
                .finally( () => {
                    commit(tiposGlobal.mutations.terminarProceso);
                } );
        } );
    },
    [tiposProfesor.actions.actualizar]: ({commit}, data) => {
        commit(tiposGlobal.mutations.iniciarProceso);
        return new Promise( (resolve, reject) => {
            Vue.http.put( 'profesor/' + data.clave, {profesor:data} )
                .then( (result) => {
                    resolve( result );
                } )
                .catch( err => {
                    reject( err );
                } )
                .finally( () => {
                    commit(tiposGlobal.mutations.terminarProceso);
                } );
        } );
    },
    [tiposProfesor.actions.eliminar]: ({commit}) => {},    
    [tiposProfesor.actions.activar]: ({commit}, data) => {
        let url = data.estado == '1' ? 'profesor/activar/' + data.clave : 'profesor/desactivar/' + data.clave;
        commit( tiposGlobal.mutations.iniciarProceso );
        return new Promise( (resolve, reject) => {
            Vue.http.put( url )
                .then( (result) => {
                    resolve( result );
                } )
                .catch( err => {
                    reject( err );
                } )
                .finally( () => {
                    commit(tiposGlobal.mutations.terminarProceso);
                } );
        } );
    },
};

const getters = {
    [tiposProfesor.getters.profesor]: (state) => {
        return state.profesor;
    },
    [tiposProfesor.getters.profesores]: (state) => {
        return state.profesores;
    },
    [tiposProfesor.getters.paginacion]: (state) => {
        return state.paginacion;
    },
    [tiposProfesor.getters.areas]: (state) => {
        return state.areas;
    },
};

const mutations = {
    [tiposProfesor.mutations.setProfesores]: (state, profesores) => {
        state.profesores = profesores.apiResponse;
    },
    [tiposProfesor.mutations.setAreas]: (state, areas) => {
        state.areas = areas.apiResponse;
    },
    [tiposProfesor.mutations.setProfesor]: (state) => {
        state.profesor = profesor;
    },
    [tiposProfesor.mutations.setPaginacion]: (state, paginacion) => {
        state.paginacion = paginacion.apiResponse;
    }
};

export default {
    state,
    actions,
    getters,
    mutations
};