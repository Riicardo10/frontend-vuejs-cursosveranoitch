import tiposGlobal from '@/tipos/global';
import tiposAuth from '@/tipos/auth';
import Vue from 'vue';

const jwtDecode = require('jwt-decode');
const tokensito = !!window.localStorage.getItem('_token') ? jwtDecode( window.localStorage.getItem('_token') ) : null;

const state = {
  usuario: tokensito,
  credenciales: window.localStorage.getItem('_user'),
  autenticado: !!window.localStorage.getItem('_token'),
  ruta: ''
};

const actions = {
  [tiposAuth.actions.login]: ({commit}, userInput) => {
    commit(tiposGlobal.mutations.iniciarProceso);
    return new Promise((resolve, reject) => {
      Vue.http.post('login', userInput)
        .then( result => {
            let tokensito = result.body.token;
            let user = JSON.stringify( result.body.usuario.usuario[0] );
            window.localStorage.setItem('_token', tokensito);
            window.localStorage.setItem('_user', JSON.stringify( JSON.parse( user ) ) );
            commit(tiposAuth.mutations.setUsuario);
            resolve(result);
        })
        .catch( error => {
          reject(error);
        })
        .finally(() => {
          commit(tiposGlobal.mutations.terminarProceso);
        })
    })
  },
  [tiposAuth.actions.logout]: ({commit}) => {
    window.localStorage.removeItem('_token');
    window.localStorage.removeItem('_user');
    commit(tiposAuth.mutations.setUsuario);
  },
  [tiposAuth.actions.registro]: ({commit}, userInput) => {
    commit(tiposGlobal.mutations.iniciarProceso);
    return new Promise((resolve, reject) => {
      Vue.http.post('usuario', {user: userInput})
        .then(user => {
          resolve(user);
        })
        .catch(error => {
          reject(error);
        })
        .finally(() => {
          commit(tiposGlobal.mutations.terminarProceso);
        })
    })
  },
};

const getters = {
  [tiposAuth.getters.ruta]: (state) => {
    return state.ruta;
  },
  [tiposAuth.getters.usuario]: (state) => {
    return state.usuario;
  },
  [tiposAuth.getters.autenticado]: (state) => {
    return state.autenticado;
  },
  [tiposAuth.getters.credenciales]: (state) => {
    let data = JSON.parse(state.credenciales);
    return data;
  }
};

const mutations = {
  [tiposAuth.mutations.setUsuario]: (state) => {
    if(window.localStorage.getItem('_token')) {
      const token = window.localStorage.getItem('_token');
      const jwtDecode = require('jwt-decode');
      state.usuario = jwtDecode(token);
      state.autenticado = true;
    }
    else {
      state.usuario = null;
      state.autenticado = false;
    }
    if(window.localStorage.getItem('_user')) {
      const user = window.localStorage.getItem('_user');
      state.credenciales = user;
    }
    else {
      state.credenciales = null;
    }
  },
  [tiposAuth.mutations.setAutenticado]: (state, autenticado) => {
    state.autenticado = autenticado;
  },
};

export default {
  state,
  actions,
  getters,
  mutations
};
