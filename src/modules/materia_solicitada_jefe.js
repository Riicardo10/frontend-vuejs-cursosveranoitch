import tiposGlobal from '@/tipos/global';
import tiposMateriaSolicitadaJefe from '@/tipos/materia_solicitada_jefe';
import Vue from 'vue';

const state = {
    materias_solicitadas_jefe: [],
    materias: [],
    profesores: [],
    paginacion: [],
    materia_solicitada: null,
    lista: []
};

const actions = {
    [tiposMateriaSolicitadaJefe.actions.fetchMateriasSolicitadasJefe]: ({commit}, data) => {
        let url = data.pagina ? 'materias_solicitadas_jefe?page=' + data.pagina : 'materias_solicitadas_jefe?page=1';
        commit( tiposGlobal.mutations.iniciarProceso );
        Vue.http.get( url )
            .then( (result) => {
                commit( tiposMateriaSolicitadaJefe.mutations.setMateriasSolicitadasJefe, { apiResponse: result.body.materias.data } );
                commit( tiposMateriaSolicitadaJefe.mutations.setPaginacion, { apiResponse: result.body.paginacion } );
            });
            commit( tiposGlobal.mutations.terminarProceso )
    },
    [tiposMateriaSolicitadaJefe.actions.fetchProfesores]: ({commit}, data) => {
        commit( tiposGlobal.mutations.iniciarProceso );
        Vue.http.get( 'profesores' )
            .then( (result) => {
                commit( tiposMateriaSolicitadaJefe.mutations.setProfesores, { apiResponse: result.body.profesores } );
            });
            commit( tiposGlobal.mutations.terminarProceso )
    },
    [tiposMateriaSolicitadaJefe.actions.aprobarMateria]: ({commit}, data) => {
        let url = data.estado == 0 ? 'materias_solicitadas_jefe/aprobar/' + data.clave + '/0/0' : 'materias_solicitadas_jefe/aprobar/' + data.clave + '/' + data.clave_profesor + '/1';
        commit( tiposGlobal.mutations.iniciarProceso );
        return new Promise( (resolve, reject) => {
            Vue.http.put( url )
                .then( (result) => {
                    resolve( result );
                } )
                .catch( err => {
                    reject( err );
                } )
                .finally( () => {
                    commit(tiposGlobal.mutations.terminarProceso);
                } );
        } );
    },
    [tiposMateriaSolicitadaJefe.actions.fetchListaMateria]: ({commit}, data) => {
        commit( tiposGlobal.mutations.iniciarProceso );
        Vue.http.get( 'lista_materia/' + data )
            .then( (result) => {
                commit( tiposMateriaSolicitadaJefe.mutations.setLista, { apiResponse: result.body.listado } );
            });
            commit( tiposGlobal.mutations.terminarProceso )
    },
};

const getters = {
    [tiposMateriaSolicitadaJefe.getters.materias]: (state) => {
        return state.materias;
    },
    [tiposMateriaSolicitadaJefe.getters.profesores]: (state) => {
        return state.profesores;
    },
    [tiposMateriaSolicitadaJefe.getters.materias_solicitadas_jefe]: (state) => {
        return state.materias_solicitadas_jefe;
    },
    [tiposMateriaSolicitadaJefe.getters.materia_solicitada]: (state) => {
        return state.materia_solicitada;
    },
    [tiposMateriaSolicitadaJefe.getters.paginacion]: (state) => {
        return state.paginacion;
    },
    [tiposMateriaSolicitadaJefe.getters.lista]: (state) => {
        return state.lista;
    },
};

const mutations = {
    [tiposMateriaSolicitadaJefe.mutations.setMaterias]: (state, materias) => {
        state.materias = materias.apiResponse;
    },
    [tiposMateriaSolicitadaJefe.mutations.setMateriasSolicitadasJefe]: (state, materias_solicitadas) => {
        state.materias_solicitadas_jefe = materias_solicitadas.apiResponse;
    },
    [tiposMateriaSolicitadaJefe.mutations.setMateriaSolicitada]: (state, materia_solicitada) => {
        state.materia_solicitada = materia_solicitada.apiResponse;
    },
    [tiposMateriaSolicitadaJefe.mutations.setProfesores]: (state, profesores) => {
        state.profesores = profesores.apiResponse;
    },
    [tiposMateriaSolicitadaJefe.mutations.setPaginacion]: (state, paginacion) => {
        state.paginacion = paginacion.apiResponse;
    },
    [tiposMateriaSolicitadaJefe.mutations.setLista]: (state, lista) => {
        state.lista = lista.apiResponse;
    }
};

export default {
    state,
    actions,
    getters,
    mutations
};
