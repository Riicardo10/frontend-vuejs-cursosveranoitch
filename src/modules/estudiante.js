import tiposGlobal from '@/tipos/global';
import tiposEstudiante from '@/tipos/estudiante';
import Vue from 'vue';

const state = {
  estudiante: null,
  estudiantes: [],
  carreras: [],
  paginacion: []
};

const actions = {
    [tiposEstudiante.actions.fetchEstudiantes]: ({commit}, data) => {
        let url = data.buscar ? 'estudiante?page=' + data.pagina + '&buscar=' + data.buscar : 'estudiante?page=' + data.pagina;        
        commit( tiposGlobal.mutations.iniciarProceso );
        Vue.http.get( url )
            .then( (result) => {
                commit( tiposEstudiante.mutations.setEstudiantes, { apiResponse: result.body.estudiantes.data } );
                commit( tiposEstudiante.mutations.setPaginacion, { apiResponse: result.body.paginacion } );
            });
            commit( tiposGlobal.mutations.terminarProceso )
    },
    [tiposEstudiante.actions.fetchCarreras]: ({commit}) => {
        commit( tiposGlobal.mutations.iniciarProceso );
        Vue.http.get( 'carreras' )
            .then( (result) => {
                commit( tiposEstudiante.mutations.setCarreras, { apiResponse: result.body.carreras } );
            });
            commit( tiposGlobal.mutations.terminarProceso )
    },
    [tiposEstudiante.actions.registrar]: ({commit}, data) => {
        commit(tiposGlobal.mutations.iniciarProceso);
        return new Promise( (resolve, reject) => {
            Vue.http.post( 'estudiante', {estudiante:data} )
                .then( (result) => {
                    resolve( result );
                } )
                .catch( err => {
                    reject( err );
                } )
                .finally( () => {
                    commit(tiposGlobal.mutations.terminarProceso);
                } );
        } );
    },
    [tiposEstudiante.actions.actualizar]: ({commit}, data) => {
        commit(tiposGlobal.mutations.iniciarProceso);
        return new Promise( (resolve, reject) => {
            Vue.http.put( 'estudiante/' + data.matricula, {estudiante:data} )
                .then( (result) => {
                    resolve( result );
                } )
                .catch( err => {
                    reject( err );
                } )
                .finally( () => {
                    commit(tiposGlobal.mutations.terminarProceso);
                } );
        } );
    },
    [tiposEstudiante.actions.eliminar]: ({commit}) => {},    
};

const getters = {
    [tiposEstudiante.getters.estudiante]: (state) => {
        return state.estudiante;
    },
    [tiposEstudiante.getters.estudiantes]: (state) => {
        return state.estudiantes;
    },
    [tiposEstudiante.getters.paginacion]: (state) => {
        return state.paginacion;
    },
    [tiposEstudiante.getters.carreras]: (state) => {
        return state.carreras;
    },
};

const mutations = {
    [tiposEstudiante.mutations.estudiantesRecibidos]: (state, estudiantes) => {
        state.estudiantes = estudiantes.apiResponse;
    },
    [tiposEstudiante.mutations.setCarreras]: (state, carreras) => {
        state.carreras = carreras.apiResponse;
    },
    [tiposEstudiante.mutations.setEstudiante]: (state) => {
        state.estudiante = estudiante;
    },
    [tiposEstudiante.mutations.setEstudiantes]: (state, estudiantes) => {
        state.estudiantes = estudiantes.apiResponse;
    },
    [tiposEstudiante.mutations.setPaginacion]: (state, paginacion) => {
        state.paginacion = paginacion.apiResponse;
    }
};

export default {
    state,
    actions,
    getters,
    mutations
};